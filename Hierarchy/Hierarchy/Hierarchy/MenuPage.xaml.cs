﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : ContentPage
    {
        public MenuPage()
        {
            InitializeComponent();
        }

        async void CountryButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Pages.ContinentPage());
        }
        
        async void SchoolButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Pages.SchoolMajor());
        }

        async void CalculatorButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Pages.Calculator());
        }
        async void RandomImageButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Pages.RandomImagePage());
        }


    }
}