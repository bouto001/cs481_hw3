﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SchoolMajor : ContentPage
    {
        public SchoolMajor()
        {
            InitializeComponent();
        }

        async void FailureButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SuccessFailure.Failure());
        }
        
        async void SuccessButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Pages.SchoolName());
        }
    }
}