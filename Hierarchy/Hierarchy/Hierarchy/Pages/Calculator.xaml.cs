﻿using Hierarchy.Calculator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Calculator : ContentPage
    {
        public Calculator()
        {
            InitializeComponent();
            InitializeComponent();
            BindingContext = this;
            OnPropertyChanged(nameof(EraseText));
        }

        public string EraseText { get; set; } = "<-";
        public string MainLabel { get; private set; } = "0";
        public string SecondaryLabel { get; private set; } = "";
        private bool HasToBeCleared = true;

        private void Button_Clicked(object sender, EventArgs e)
        {
            if (HasToBeCleared)
            {
                MainLabel = "";
                HasToBeCleared = false;
            }

            MainLabel += (sender as Button).Text;
            OnPropertyChanged(nameof(MainLabel));
        }

        private void EqualButton_Clicked(object sender, EventArgs e)
        {
            var tmp = MainLabel;

            MainLabel = Calc.ComputeCalculation(MainLabel);
            SecondaryLabel = tmp;

            OnPropertyChanged(nameof(MainLabel));
            OnPropertyChanged(nameof(SecondaryLabel));
        }

        private void DiscardButton_Clicked(object sender, EventArgs e)
        {
            MainLabel = "0";
            SecondaryLabel = "";
            HasToBeCleared = true;
            OnPropertyChanged(nameof(MainLabel));
            OnPropertyChanged(nameof(SecondaryLabel));
        }

        private void EraseButton_Clicked(object sender, EventArgs e)
        {
            if (MainLabel.Length == 0)
                return;
            else if (MainLabel.Length == 1)
            {
                HasToBeCleared = true;
                MainLabel = "0";
            }
            else
                MainLabel = MainLabel.Substring(0, MainLabel.Length - 1);

            OnPropertyChanged(nameof(MainLabel));
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await DisplayAlert("Alert", "New feature: operand on an answer", "Got it !");
        }

        protected async override void OnDisappearing()
        {
            base.OnDisappearing();
            await DisplayAlert("Alert", "See you soon", "Thanks");
        }

    }
}