﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RandomImagePage : ContentPage
    {
        public RandomImagePage()
        {
            InitializeComponent();
            BindingContext = this;

            ImageSource = "https://picsum.photos/1000/1000?q=" + DateTime.Now.ToUniversalTime().Subtract(
               new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
            OnPropertyChanged(nameof(ImageSource));
        }

        public string ImageSource { get; set; }

        private void Button_Clicked(object sender, EventArgs e)
        {
            ImageSource = "https://picsum.photos/1000/1000?q=" + DateTime.Now.ToUniversalTime().Subtract(
                new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
            OnPropertyChanged(nameof(ImageSource));
        }
    }
}