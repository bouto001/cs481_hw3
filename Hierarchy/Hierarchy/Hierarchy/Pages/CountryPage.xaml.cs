﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CountryPage : ContentPage
    {
        public CountryPage()
        {
            InitializeComponent();
        }

        async void Failure_ButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SuccessFailure.Failure());
        }

        async void Success_ButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SuccessFailure.Success());
        }
    }
}