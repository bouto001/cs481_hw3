﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy.SuccessFailure
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Failure : ContentPage
    {
        public Failure()
        {
            InitializeComponent();
        }

        async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopToRootAsync();
        }

        async void GobackButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}