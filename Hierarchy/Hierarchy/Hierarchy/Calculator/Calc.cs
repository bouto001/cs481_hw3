﻿using System;
using System.Data;

namespace Hierarchy.Calculator
{
    public static class Calc
    {
        public static string ComputeCalculation(string input)
        {
            try
            {
                var pureInput = input.Replace("x", " * ");
                var res = double.Parse(new DataTable()
                    .Compute(pureInput, null).ToString());
                    

                return res.ToString();
            }
            catch (Exception ex)
            {
                var r = ex.Message;
                return "ERROR";
            }
        }
    }
}
